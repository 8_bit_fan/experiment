from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class SoilReading(Resource):    
    def post(self):
        soil_reading = request.form['data']
        #Store into the database
        #maybe use sql alchemy to store it?

        water_reading = soil_reading['water']

    def get(self):
        #Get the latest reading from the database and return it
        #this is in volts.
        return {'date_time': '2019-01-01', 'reading': '45.0'}

class WindReading(Resource):
    def post(self):
        wind_speed = request.form['data']

    def get(self):
        return {'date_time': '2019-01-01', 'reading': '15', 'direction': 'north'}

class TemperatureReading(Resource):
    def post(self):
        temp = request.form['data']

    def get(self):
        return {'date_time': '2019-01-01', 'reading': 55'}

api.add_resource(SoilReading, '/')

if __name__ == '__main__':
    app.run(debug = True)


